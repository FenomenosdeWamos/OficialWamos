const express = require('express');
const exphbs = require('express-handlebars');
const app = express();

app.engine('.hbs', exphbs({
  defaultLayout: 'template',
  extname: '.hbs',
  partialsDir: ['views/Partials']
}));

app.set('view engine', '.hbs');
app.use(express.static('recursos'));
app.get('/', (req, res) => {
  res.render('index');
});

app.use(express.static ('public'));

app.listen(3000, () => {
  console.log('El puerto esta ready en 3000 para Wamos los fenómenos')
});
